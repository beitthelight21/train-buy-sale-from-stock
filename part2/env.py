import pyglet
import time 


class StockEnv(object):
    viewer = None

    def __init__(self):
        pass

    def step(self, action):
        pass

    def reset(self):
        pass

    def render(self):
        if self.viewer is None:
            self.viewer = Viewer()
        self.viewer.render()


class Viewer(pyglet.window.Window):

    def __init__(self):
        # vsync=False to not use the monitor FPS, we can speed up training
        super(Viewer, self).__init__(width=400, height=400, resizable=False, caption='Stock', vsync=False)
        pyglet.gl.glClearColor(1, 1, 1, 1)


        self.money = 100000
        self.lot   = 0
        self.profit = 0
        self.batch = pyglet.graphics.Batch()    # display whole batch at once
        self.label1 = pyglet.text.Label (str(self.money), batch=self.batch, color=(0,0,0,255), x = self.width/4, y = self.height * 3 / 4, anchor_x='center', anchor_y='center')
        self.label2 = pyglet.text.Label ('profit', batch=self.batch, color=(0,0,0,255), x = self.width*3/4, y = self.height * 3 / 4, anchor_x='center', anchor_y='center')

    def render(self):
        self.update_money()
        self.switch_to()
        self.dispatch_events()
        self.dispatch_event('on_draw')
        self.flip()

    # 
    # 股票成本 = 股價 x 1,000(股) x 張數
    # 所以我們統整以上數值，股票手續費的公式為：
    # 股票交易手續費 = (股票買進成本 x 0.1425% x 券商折扣) + (股票賣出成本 x 0.1425% x 券商折扣) + (股票賣出成本 x 0.3%)
    # 
    # def buy(self):
    #     self.lot += 1

    # def sale(self):
        

    def on_draw(self):
        self.clear()
        self.batch.draw()

    def update_money(self):
        self.money += 1000
        self.label1.text = str(self.money)
        self.label2.text = str(self.profit)
        
if __name__ == '__main__':
    env = StockEnv()
    while True:
        env.render()
        time.sleep(1)
